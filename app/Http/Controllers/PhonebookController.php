<?php

namespace App\Http\Controllers;

use App\Phonebook;
use Illuminate\Http\Request;
//use App\Http\Requests\PhonebookRequest;
use App\Repositories\PhonebookRepository;
use Validator;

class PhonebookController extends Controller
{
    const PAGINATION_DEFAULT_LIMIT = 10;
	
    /**
     * Create a new PhonebookController instance.
     *
     * @param  \App\Repositories\PhonebookRepository $repository
     */
    public function __construct(PhonebookRepository $repository)
    {
        $this->repository = $repository;
    }
	
    /**
     * Display a listing of the resource.
     *
	 * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int)$request->limit ? (int)$request->limit : $this::PAGINATION_DEFAULT_LIMIT;
		$search = $request->search ? $request->search : '';

        try {
            $entries = $this->repository->search($limit, $search);
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        return response()->json($entries, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->post(), [
            'name' => 'bail|required|max:191',
			'entrytype' => 'bail|in:mobilni,poslovni,fiksni',
			'number' => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }	
	
        $phonebook = $this->repository->store($request);
        return response()->json($phonebook, 201);		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Phonebook  $phonebook
     * @return \Illuminate\Http\Response
     */
    public function show(Phonebook $phonebook)
    {
		return $phonebook;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Phonebook  $phonebook
     * @return \Illuminate\Http\Response
     */
    public function edit(Phonebook $phonebook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Phonebook  $phonebook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Phonebook $phonebook)
    {
        $validator = Validator::make($request->post(), [
		    'id' => 'bail|required|integer|min:1',
            'name' => 'bail|required|max:191',
			'entrytype' => 'bail|in:mobilni,poslovni,fiksni',
			'number' => 'required|max:191',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
		
        $this->repository->update($phonebook, $request);
        return response()->json($phonebook, 200);
    }

    /**
     * Remove the specified resource from storage.
     *	 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {		
        try {		
            $this->repository->destroy($request);
        } catch(\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }		
		return response()->json(null, 204);
    }
}
