<?php

namespace App\Repositories;

use App\Phonebook;

class PhonebookRepository
{
    /**
     * Get posts with search.
     *
     * @param  int  $n
     * @param  string  $search
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search($n, $search)
    {
        return Phonebook::where('name', 'like', "%$search%")
            ->paginate($n);
    }
	
    /**
     * Update entry.
     *
     * @param  \App\Phonebook  $phonebook
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function update($phonebook, $request)
    {
        $phonebook->update($request->post());
		return $phonebook;
    }

    /**
     * Store entry.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function store($request)
    {
        $phonebook = Phonebook::create($request->post());
		return $phonebook;		
    }
	
    /**
     * Remove entry.
     *
     * @param  \App\Http\Requests\PhonebookRequest  $request
     * @return void
     */
    public function destroy($request)
    {
        $phonebook = Phonebook::where('name', $request->name)->orWhere('number', $request->number)->firstOrFail();
		$phonebook->delete();
    }	
}