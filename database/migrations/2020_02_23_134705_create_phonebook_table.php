<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonebookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phonebook', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
			$table->enum('entrytype', ['mobilni', 'poslovni', 'fiksni']);
			$table->string('number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('phonebook', function (Blueprint $table) {
            //
        });*/
		Schema::dropIfExists('phonebook');
    }
}
